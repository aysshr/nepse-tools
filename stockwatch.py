COMPANIES = [
    "DDBL",
    "CBBL",
    "SWBBL",
    "NUBL",
    "NIL",
    "NLG",
    "PIC",
    "HDL",
    "SHIVM",
    "NLIC",
    "ALICL",
    "NLICL",
    "PRVU",
    "ADBL",
    "NICA",
    "PCBL",
    "EBL",
    "HBL",
]

from bs4 import BeautifulSoup
import csv
from datetime import datetime
import logging as log
import openpyxl
import os
import requests

# Only these fields will be rendered in the output xls, csv
FIELD_NAMES = [
    "Company",
    "EPS",
    "P/E Ratio",
    "Book Value",
    "% Bonus",
    "Bonus Average",
    "% Dividend",
    "Dividend Average",
    "Graham Number",
    # "PGN ratio",
    "Shares Outstanding",
    "Market Price",
    "% Change",
]
div_bonus_table_id = {
    "dividend-panel": "Dividend Average",
    "bonus-panel": "Bonus Average"
}


def get_company_details_from_meroshare(company_symbol):
    """
    Scrape the company details from mero share, parse & extract pertinent information
    """
    company_details = {}
    params = (("symbol", company_symbol),)
    response = requests.get("https://merolagani.com/CompanyDetail.aspx", params=params)
    page = response.content
    soup = BeautifulSoup(page, "html.parser")

    details_panel = soup.find("table", {"class": "table-striped", "id": "accordion"})
    table_rows = details_panel.find_all("tr")
    for row in table_rows:
        if row.get("id"):
            if str(row["id"]) in div_bonus_table_id:
                div_bonus = {}
                for r in row.find_all("tr"):
                    datas = r.find_all("td")
                    if len(datas) < 3:
                        continue
                    year = str(datas[2].text).strip()
                    percent  = get_numeric_value(datas[1].text)
                    if percent:
                        div_bonus[year] = percent
                if div_bonus:
                    company_details[str(row["id"])] = div_bonus
        else:
            th = row.find("th")
            td = row.find("td")
            if th and td:
                th_text = th.text.strip()
                td_text = td.text.strip()
                td_text = " ".join(td_text.split())
                td_number = get_numeric_value(td_text)
                if td_number:
                    td_text = td_number
                company_details[th_text] = td_text
    return company_details


def add_extra_company_info(details):
    """ Add extra information on the company details """
    PE_ratio = float(details.get("P/E Ratio"))
    book_value = float(details.get("Book Value"))
    price = float(details.get("Market Price"))
    if PE_ratio and book_value and price:
        graham_number = int((PE_ratio * book_value * 22.5) ** (1 / 2))
        details["Graham Number"] = graham_number
        # details["PGN ratio"] = float("{:.2f}".format(price / graham_number))

    for panel, repr_name in div_bonus_table_id.items():
        if details.get(panel):
            number = total = 0
            for year, percent in details[panel].items():
                if year and percent:
                    number += 1
                    total += float(percent)
            if number and total:
                details[repr_name] = "{:.2f} ({} years)".format(total/number, number)


def is_number(string):
    try:
        float(string.replace(",", ""))
        return True
    except ValueError:
        return False

def get_numeric_value(text):
    text = text.replace("%", "")
    td_pretext = text.strip().replace(",", "").split(" ")[0]
    if is_number(td_pretext):
        return float(td_pretext)
    return None

def dump_to_excel(company_datas):
    workbook = openpyxl.Workbook()
    sheet = workbook.active

    for i, column_name in enumerate(FIELD_NAMES, 1):
        sheet.cell(row=1, column=i, value=column_name)
    row = 2
    for company_data in company_datas:
        for key, value in company_data.items():
            if key in FIELD_NAMES:
                column = FIELD_NAMES.index(key) + 1
                sheet.cell(row=row, column=column, value=value)
        row += 1

    now = datetime.now()
    dt_string = now.strftime("%Y-%m-%d %H-%M-%S")
    file_name = "companydatas/{}.xlsx".format(dt_string)
    workbook.save(filename=file_name)


def dump_to_csv(company_datas):
    with open("companydata.csv", "w") as csv_file:
        writer = csv.DictWriter(csv_file, fieldnames=FIELD_NAMES)
        writer.writeheader()
        for company_data in company_datas:
            details = {k: company_data[k] for k in company_data if k in FIELD_NAMES}
            writer.writerow(details)


if __name__ == "__main__":
    company_datas = []
    for company in COMPANIES:
        try:
            company_data = {"Company": company}
            company_data.update(get_company_details_from_meroshare(company))
            add_extra_company_info(company_data)
            company_data = {
                k: company_data[k] for k in company_data if k in FIELD_NAMES
            }
            company_datas.append(company_data)
        except:
            log.exception("Error extracting details of: {}".format(company))
    dump_to_excel(company_datas)
    # dump_to_csv(company_datas)
