package main

import (
	"crypto/tls"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"sync"
)

func GetCompanyCodes(httpClient *http.Client) []string {
	flag.Parse()
	companySymbols := flag.Args()
	req, err := http.NewRequest("GET", "https://newweb.nepalstock.com/api/nots/security?nonDelisted=true", nil)
	req.Header.Add("user-agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 11_0_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.192 Safari/537.36")
	resp, err := httpClient.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	bodyText, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	var allSecurities []interface{}
	json.Unmarshal([]byte(bodyText), &allSecurities)
	validCompanyIDs := []string{}

	for _, security := range allSecurities {
		securityDetails := security.(map[string]interface{})
		securitySymbol := securityDetails["symbol"].(string)
		for _, companySymbol := range companySymbols {
			if strings.ToUpper(companySymbol) == strings.ToUpper(securitySymbol) {
				securityID := securityDetails["id"].(float64)
				securityIDInt := int(securityID)
				validCompanyIDs = append(validCompanyIDs, fmt.Sprint(securityIDInt))
			}
		}
	}
	return validCompanyIDs
}

func GetSecurityMarketDepth(securityID string, httpClient *http.Client) (map[string]float64, error) {
	marketDepthRes := make(map[string]float64)
	marketDepthURL := fmt.Sprintf("https://newweb.nepalstock.com/api/nots/nepse-data/marketdepth/%s", securityID)
	req, err := http.NewRequest("GET", marketDepthURL, nil)
	req.Header.Add("accept", "application/json")
	req.Header.Add("user-agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 11_0_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.192 Safari/537.36")

	resp, err := httpClient.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	bodyText, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	if len(bodyText) > 0 {
		var result map[string]interface{}
		json.Unmarshal([]byte(bodyText), &result)
		marketDepthRes["buyQty"] = result["totalBuyQty"].(float64)
		marketDepthRes["sellQty"] = result["totalSellQty"].(float64)
	}
	return marketDepthRes, nil
}

func GetSecurityData(securityID string, httpClient *http.Client) {
	todayDataURL := fmt.Sprintf("https://newweb.nepalstock.com/api/nots/security/%s", securityID)
	req, err := http.NewRequest("GET", todayDataURL, nil)
	req.Header.Add("accept", "application/json")
	req.Header.Add("user-agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 11_0_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.192 Safari/537.36")

	resp, err := httpClient.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	bodyText, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	var result map[string]interface{}
	json.Unmarshal([]byte(bodyText), &result)

	todayData := result["securityDailyTradeDto"].(map[string]interface{})
	securityDetails := result["security"].(map[string]interface{})

	LTP := todayData["lastTradedPrice"].(float64)
	previousClose := todayData["previousClose"].(float64)

	marketDepthRes, err := GetSecurityMarketDepth(securityID, httpClient)
	buyQty := 0.0
	sellQty := 0.0
	if err == nil {
		buyQty = marketDepthRes["buyQty"]
		sellQty = marketDepthRes["sellQty"]
	}
	fmt.Printf(
		"\n%s, LTP:%v (%v), OPEN:%v, HIGH:%v, LOW:%v, CLOSE:%v, %v:%v\n",
		securityDetails["symbol"],
		LTP,
		LTP-previousClose,
		todayData["openPrice"],
		todayData["highPrice"],
		todayData["lowPrice"],
		previousClose,
		buyQty,
		sellQty,
	)
}

func main() {
	tr := &http.Transport{
		TLSClientConfig:   &tls.Config{InsecureSkipVerify: true},
		DisableKeepAlives: false,
	}
	client := &http.Client{Transport: tr}
	companyIDs := GetCompanyCodes(client)
	var wg sync.WaitGroup
	for _, securityID := range companyIDs {
		wg.Add(1)
		secID := securityID
		go func() {
			GetSecurityData(secID, client)
			wg.Done()
		}()
	}
	wg.Wait()
}
