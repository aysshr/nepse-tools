# Nepse-tools
Few helper scripts to extract necessary stock info from new-web nepse & merolagani!
**Please don't overabuse these scripts to Over-request nepse site & merolagani**

1. To get today's share prices of only the companies you want
    **From binary**:
    ``` bin/nepse-daily <company1 symbol> <company2 symbol>```
    eg: ``` bin/nepse-daily adbl prvu nlic```

    **From python**:
    ```python nepse.py -c adbl prvu ddbl nlic```
    eg: ```python nepse.p -c adbl prvu ddbl nlic```

2. To scrape fundamental statistics of Companies to an excel sheet
    - Run the script from home ```python stockwatch.py```
    - Go inside `companydata` folder & view the dumpled result in the excel file
    (Note: Edit in `stockwatch.py` to add your companies)
# Note
Make sure to install these python packages
```pip install beautifulsoup4 openpyxl requests```