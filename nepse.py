"""
Get recent trading figures of the companies you wish

usage: python nepse.p -c adbl prvu ddbl nlic
"""


import argparse
import logging as log
import re
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
import sys
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

NEPSE_MARKETDEPTH = "https://newweb.nepalstock.com/api/nots/security"
NEPSE_SECURITYLIST = "https://newweb.nepalstock.com/api/nots/security"
HEADERS = {
    'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_0_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.192 Safari/537.36',
}


def parseargs(args):
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-c",
        "--companies",
        type=str,
        nargs="+",
        help="symbols of companies (eg. ddbl, nabil)",
        required=True,
    )
    return parser.parse_args()


def get_company_codes(company_symbols):
    """ Get the numeric code of the companies provided in the argument """
    company_symbols = [sym.upper() for sym in company_symbols]
    securities_response = requests.get(
        NEPSE_SECURITYLIST,
        headers=HEADERS,
        params=(("nonDelisted", True),)
    )
    if securities_response.status_code != 200:
        print("Could not extract securities list, exiting!")
        sys.exit(1)
    all_securities = securities_response.json()
    valid_ids = []
    for security in all_securities:
        if security["symbol"].upper() in company_symbols:
            valid_ids.append(security["id"])
    return valid_ids


def get_market_depth(company_code):
    marketdepth_response = requests.get(
        '{}/{}'.format(NEPSE_MARKETDEPTH, company_code),
        headers=HEADERS
    )
    if marketdepth_response.status_code != 200:
        print(marketdepth_response.status_code)
        return
    result = marketdepth_response.json()
    security_details = result['security']
    today_data = result['securityDailyTradeDto']
    print(
        "\n{symbol}, LTP:{ltp} ({diff}), OPEN:{open}, HIGH:{high}, LOW:{low}, CLOSE:{close}".format(
            symbol=security_details['symbol'],
            ltp=int(today_data['lastTradedPrice']),
            diff=int(today_data['lastTradedPrice'] -
                     today_data['previousClose']),
            open=int(today_data['openPrice']),
            close=int(today_data.get('previousClose')),
            high=int(today_data['highPrice']),
            low=int(today_data['lowPrice']),
        )
    )


if __name__ == "__main__":
    args = parseargs(sys.argv[1:])
    company_codes = get_company_codes(args.companies)
    for company in company_codes:
        try:
            get_market_depth(company)
        except:
            print("\n Error getting details for: {}".format(company))
